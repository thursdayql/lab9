
public class Utility
{

  // Exercise1
  //  Write a method called sumArray that accepts an array of floating point values 
  //  and returns the sum of the values stored in the array.

  public double sumArray(double[] dArray)
  {
    double sum = 0.0;

    if (dArray == null || dArray.length == 0)
      return Double.NaN;

    for (int index = 0; index < dArray.length; index++)
    {
      sum = sum + dArray[index];
    }

    return sum;
  }


  // Exercise2
  //  Write a method called productArray that accepts an array of floating point 
  //  values and returns the product of the values stored in the array.

  public double productArray(double[] dArray)
  {
    double product = 1.0;

    if (dArray == null || dArray.length == 0)
      return Double.NaN;

    for (int index = 0; index < dArray.length; index++)
    {
      product *= dArray[index];
    }

    return product;
  }


  // Exercise3
  //  Write a method called switchThem that accepts two integer arrays as parameters 
  //  and switches the contents of the arrays. Take into account that the arrays may 
  //  be of different sizes.

  public void switchThem(int[] arrayOne, int[] arrayTwo)
  {
    int smallerLength = arrayOne.length;

    if (arrayOne.length > arrayTwo.length)
      smallerLength = arrayTwo.length;

    for (int index = 0; index < smallerLength; index++)
    {
      int tmp = arrayOne[index];
      arrayOne[index] = arrayTwo[index];
      arrayTwo[index] = tmp;
    }
  }

  public String displayIntArray(int[] iArray)
  {
    String str = "";
    for (int iValue : iArray)
      str += iValue + "  ";

    return str + "\n";
  }

  public String displayDoubleArray(double[] dArray)
  {
    String str = "";
    for (double dValue : dArray)
      str += dValue + "  ";

    return str + "\n";
  }


  // Exercise4
  //  Write a method called maxArrayElement that accepts an array of floating point 
  //  values and returns the the largest value stored in the array.

  public double maxArrayElement(double[] dArray)
  {
    double max = dArray[0];

    for (int index = 0; index < dArray.length; index++)
    {
      // max = Math.max(max,  dArray[index]);

      if (dArray[index] > max)
        max = dArray[index];
    }

    return max;
  }


  // Exercise6
  //  Write a method called reverseArray that accepts an array of floating point 
  //  values and reverses positions of all elements in  the array. 
  
  public void reverseArray(double[] dArray)
  {
    for (int index = 0; index < dArray.length / 2; index++)
    {
      double tmp = dArray[index];
      dArray[index] = dArray[dArray.length -1 - index];
      dArray[dArray.length -1 - index] = tmp;
    }
  }
  
  
  // Exercise7 (NOT WORKING)
  //  Write a method called multiplyThem that accepts two integer arrays as
  //  parameters and returns a new array containing products of elements at the 
  //  same position.  Take into account that the arrays may be of different sizes.
  
  public int[] multiplyThem(int[] iFirstAr, int[] iSecondAr)
  {
    int smallerLength = Math.min(iFirstAr.length, iSecondAr.length);
    int[] results = new int[smallerLength];
    
    for (int index = 0; index < smallerLength; index++)
      results[index] = iFirstAr[index] * iSecondAr[index];
    
    return results;
  }
}
