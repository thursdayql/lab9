//********************************************************************
//File:         Driver.java       
//Author:       Liam Quinn
//Date:         November 9th, 2017
//Course:       CPS100
//
//Problem Statement:
//  
//
//Inputs:   
//Outputs:  
// 
//********************************************************************


public class Driver
{

  public static void main(String[] args)
  {
    Utility util = new Utility();
    double[] dNumbers = {1.0, 2.0, 3.0, 4.0};
    System.out.println(util.sumArray(dNumbers));
    System.out.println(util.productArray(dNumbers)); 
    
    System.out.println();
    
    int[] ids = { 1, 2, 3, 4};
    int[] iNumbers = { 2, 2, 2, 2, 2, 2, 2};
    System.out.println(util.displayIntArray(ids));
    System.out.println(util.displayIntArray(iNumbers));
    
    util.switchThem(ids, iNumbers);
    
    System.out.println(util.displayIntArray(ids));
    System.out.println(util.displayIntArray(iNumbers));
    
    System.out.println(util.maxArrayElement(dNumbers));
    
    util.reverseArray(dNumbers);
    
    System.out.println(util.displayDoubleArray(dNumbers));
    
    util.displayIntArray(util.multiplyThem(ids, iNumbers));
    System.out.println(util.displayIntArray(util.multiplyThem(ids, iNumbers)));
  }

}
