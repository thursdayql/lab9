
import java.io.*;
import java.util.*;


public class FileNumberReverser
{
  int[] numbers = null;
  int size = -1;

  public FileNumberReverser()
  {
    numbers = null;
    size = -1;
  }

  public int countNumbersInFile(String fileName) throws IOException
  {
    Scanner fileScanner = new Scanner(new File(fileName));
    this.size = 0;

    while (fileScanner.hasNextLine())
    {
      int iValue = fileScanner.nextInt();
      size++;
    }

    fileScanner.close();
    return size;
  }

  public void readFromFileToArray(String fileName) throws IOException
  {
    Scanner fileScanner = new Scanner(new File(fileName));
    this.numbers = new int[this.size];

    int index = 0;
    while (fileScanner.hasNextLine())
    {
      this.numbers[index] = fileScanner.nextInt();
      index++;
    }
    fileScanner.close();
  }

  public String showInReverse()
  {
    System.out.println("LENGTH: " + this.numbers.length);
    String str = "";

    for (int index = this.numbers.length - 1; index >= 0; index--)
      str += "  " + this.numbers[index];

    return str;
  }
}
