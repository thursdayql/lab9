//********************************************************************
//File:         Driver.java       
//Author:       Liam Quinn
//Date:         November 9th, 2017
//Course:       CPS100
//
//Problem Statement:
//  Problem1
//    Write a Java program that reads integer numbers from a file (one 
//    number per line) into an array. Then display the numbers in 
//    reverse order.
//
//Inputs:   integer numbers in a file
//Outputs:  display numbers in a reverse order
// 
//********************************************************************
import java.io.*;
public class Driver
{

  public static void main(String[] args) throws IOException
  {
    final String FILENAME = "data.txt";
    FileNumberReverser reverser = new FileNumberReverser();
    
    // 1. Counter number of integers in file
    reverser.countNumbersInFile(FILENAME);
    
    // 2. Read numbers from the file into an array
    reverser.readFromFileToArray(FILENAME);
    
    // 3. Display the numbers in a array in a reverse order
    System.out.println(reverser.showInReverse());
  }

}
